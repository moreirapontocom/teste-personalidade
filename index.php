<?php include('header.php') ?>

	<div class="text-center">
		<div class="row">
			<div class="col col-xs-12 col-md-6 col-md-offset-3">
				<img src="img/title.png" alt="Equipe Descomplicada" class="img-responsive margin-bottom-60">

				<div class="margin-bottom-60 text-justify">
					<p>
						Conhecer melhor a si e suas características tornam-se altamente estratégicos para suas relações e seus objetivos, sejam pessoais ou profissionais. Para isto não basta de características superficiais. Avaliar as principais tendências comportamentais e conhecer melhor as suas habilidades. Permite ainda ter uma melhor compreensão de como relaciona, trabalha e como funciona.
					</p>
					<p>
						A avaliação abaixo é uma sinopse de vários estudos comportamentais e tem como objetivo sinalizar a tendência do seu padrão de comportamentos, temperamentos e facilitar o seu autoconhecimento.
					</p>
				</div>
			</div>
		</div>
	</div>

	<hr class="margin-bottom-40">

	<h1 class="margin-bottom-40">TESTE</h1>

	<form method="post" action="process.php">

		<?php
		$i = 0;
		foreach ( $teste as $item ) :
			?>

			<h4>
				<?php echo $item['pergunta'] ?> <i class="fa fa-check text-primary hidden" id="icon-<?php echo ($i+1); ?>"></i>
			</h4>
			<ul class="list-unstyled">

				<?php
				for ( $alt=0;$alt<4;++$alt ) :
					?>

					<li>
						<label for="alt_<?php echo $i.$alt ?>" data-i="<?php echo ($i+1) ?>">
							<input type="radio" name="<?php echo $i ?>" id="alt_<?php echo $i.$alt ?>" value="<?php echo $item['correspondencia_caracteristica'][$alt] ?>" /> <?php echo $item['alternativas'][$alt] ?>
							<?php //echo $item['correspondencia_caracteristica'][$alt] ?>
							<?php
							/*
							switch ($item['correspondencia_caracteristica'][$alt]) {
								case '0':
									echo 'Orientador';
									break;
								case '1':
									echo 'Organizador';
									break;
								case '2':
									echo 'Desafiador';
									break;
								case '3':
									echo 'Visionário';
									break;
							}
							*/
							?>
						</label>
					</li>

					<?php
				endfor;
				?>

			</ul>

			<hr>

			<?php
			++$i;
		endforeach;
		?>

		<button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-check"></i> Ver resposta</button>

	</form>

<?php include('footer.php') ?>
