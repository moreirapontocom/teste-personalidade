<?php

/**
 * Características possíveis
 * Sempre 4 características
*/

$caractetisticas = array(
	'Orientador', // 0
	'Organizador', // 1
	'Desafiador', // 2
	'Visionário' // 3
);

$imagens = array(
	'orientador', // 0
	'organizador', // 1
	'desafiador', // 2
	'visionario' // 3
);

$descricao = array(
	// Orientador
	'Você tem traços comportamentais que tendem a ser uma pessoa sensível, que dá muita importância para o seu time ou grupo social, preza por bons relacionamentos e a harmonia com todos ao seu redor.
		<br>
		O que faz a diferença em você é a sua comunicação, capacidade de desenvolver e liderar equipes e manter harmonia em grupos. Entretanto o que pode ser pontos de melhorias nos seus comportamentos são as manipulações, tentar esconder conflitos nas relações, controles emocionais e relação com o tempo.
		<br>
		O que te motiva é a aceitação social, segurança e reconhecimento da equipe. Valor para você é o relacionamento',

	// Organizador
	'Você tem traços comportamentais que tendem a ser detalhista, organizado, estrategista, pontual e conservador.
		<br>
		O que faz a diferença em você é a organização, consistência, qualidade e lealdade. Entretanto o que pode ser pontos de melhorias nos seus comportamentos é a dificuldade de se adaptar a mudanças, detalhista e metódico excessivamente.
		<br>
		O que te motiva é a certeza da compreensão das regras, ausência de erros e o conhecimento específico.
		<br>
		Valor para você é a Organização',

	// Desafiador
	'Você tem traços comportamentais que tendem a ser uma pessoa de atitude. Apresenta senso de urgência é prático, impulsivo, gosta de vencer desafios e ser auto suficiente O que faz a diferença em você é a facilidade que tem em fazer acontecer. Entretanto o que pode ser pontos de melhorias nos seus comportamentos é descomplicar relacionamentos e ser mais paciente e sociável.
		<br>
		O que te motiva é o controle das suas próprias atividades, desafios para vencer e variedade de tarefas.
		<br>
		Valor para você é a qualificação.',

	// Visionario
	'Você tem traços comportamentais que tendem a ser intuitivo, criativo, distraído nos mais diversos contextos, idealizam futuros, informal, flexível nas relações e nas formas de pensar. O que faz a diferença em você é a facilidade de provocar mudanças. Entretanto o que pode ser pontos de melhorias nos seus comportamentos é a rebeldia ou o excesso de autenticidade. O que te motiva é liberdade de expressão, ausência de controle rígido.
		<br>
		Valor para você é a justiça'
);

$teste = array(
	array(
		'pergunta' => 'Geralmente meu critério de escolha é:',
		'alternativas' => array(
			'Orientada por pessoas',
			'Orientado por informação',
			'Orientado por atividade',
			'Orientado por intuição'
		),
		'correspondencia_caracteristica' => array(
			0, // Orientador
			1, // Organizador
			2, // Desafiador
			3, // Visionário
		)
	),
	array(
		'pergunta' => 'O aprendizado para mim deve ser:',
		'alternativas' => array(
			'Informações seqüenciadas',
			'Informações com lógica',
			'Informações com interação',
			'Informações que envolvam pessoas'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			3, // Visionário
			0, // Orientador
		)
	),
	array(
		'pergunta' => 'Eu tenho mais facilidade de aprendizado quando:',
		'alternativas' => array(
			'O estudo não avança enquanto não finalizar a etapa',
			'Quando ensinam-me mostrando com embasamento e lógica',
			'Quando expõem a harmonia do conjunto composto de todas as diferenças',
			'Quando combinam as coisas e colocam em prática'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			0, // Orientador
			3, // Visionário
		)
	),
	array(
		'pergunta' => 'Eu avalio os erros',
		'alternativas' => array(
			'Quando há correções',
			'Pelas regras ou leis',
			'De acordo com a cooperação',
			'Pelo o que é justo'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			0, // Orientador
			3, // Visionário
		)
	),
	array(
		'pergunta' => 'Na minha conversa geralmente',
		'alternativas' => array(
			'A minha fala é calma',
			'Com freqüência entro em um timbre agudo',
			'Gesticulo muito e utilizo de todas as características',
			'Tenho uma fala provocante ou com humor'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			0, // Orientador
			3, // Visionário
		)
	),
	array(
		'pergunta' => 'O tempo para mim eu posso associar',
		'alternativas' => array(
			'Um passo de cada vez',
			'Direto, aqui e agora',
			'Exijo tempo',
			'Perco-me no tempo, vou experimentando até chegar ao fim'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			0, // Orientador
			3, // Visionário
		)
	),
	array(
		'pergunta' => 'O que geralmente prezo no relacionamento',
		'alternativas' => array(
			'Reconhecimento',
			'Quailificação',
			'Harmonia',
			'Justiça'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			0, // Orientador
			3, // Visionário
		)
	),
	array(
		'pergunta' => 'Se uma frase me resumisse, qual seria?',
		'alternativas' => array(
			'“O que devo fazer”?',
			'“A respeito do que você está falando”?',
			'“Quem estará presente”?',
			'“Toma lá, dá cá”'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			0, // Orientador
			3, // Visionário
		)
	),
	array(
		'pergunta' => 'Como você fica sabendo das coisas?',
		'alternativas' => array(
			'Pela forma e caráter ideal ou intrínseco',
			'Rotulo, classifico ou coloco em ranking',
			'Faço imagens',
			'Descubro conexões e correlações'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			3, // Visionário
			0, // Orientador
		)
	),
	array(
		'pergunta' => 'Qual é o melhor modo de trabalho?',
		'alternativas' => array(
			'Em dupla',
			'Sozinho',
			'Em grupo',
			'Sozinho ou conectado'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			0, // Orientador
			3, // Visionário
		)
	),
	array(
		'pergunta' => 'Como você pensa no processo?',
		'alternativas' => array(
			'Do detalhe ao detalhe até chegar ao todo; esquema seqüencial linear.',
			'Parte à luz do todo; rede de estruturação cristalina. ',
			'Tudo é parte de alguma outra coisa; compara e contrasta',
			'Do todo para o todo; fluxo de idéias'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			0, // Orientador
			3, // Visionário
		)
	),
	array(
		'pergunta' => 'Geralmente não tenho a paciência',
		'alternativas' => array(
			'Tenho a tendência de achar que nada é óbvio',
			'Não tolero erros pequenos',
			'Consigo facilmente o resultado de outras pessoas',
			'Provoco para ter respostas autênticas'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			0, // Orientador
			3, // Visionário
		)
	),
	array(
		'pergunta' => 'O relacionamento ideal é na seguinte forma',
		'alternativas' => array(
			'Um complemento como meu par ou ajudador',
			'Um parceiro competidor',
			'Harmonioso independente das diferenças',
			'Conexão e troca recíproca'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			0, // Orientador
			3, // Visionário
		)
	),
	array(
		'pergunta' => 'Reconheço que:',
		'alternativas' => array(
			'Faço uma enorme cobrança interna de mim mesmo',
			'Sou competidor e busco ser o primeiro',
			'Busco  a valorização do respeito das pessoas',
			'Tenho baixa ansiedade e tenho uma tendência a me preparar pouco'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			0, // Orientador
			3, // Visionário
		)
	),
	array(
		'pergunta' => 'Me deixa muito mal',
		'alternativas' => array(
			'De não estar correto',
			'De não ser o melhor',
			'De perder o respeito das pessoas',
			'Daquilo que não for justo'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			0, // Orientador
			3, // Visionário
		)
	),
	array(
		'pergunta' => 'A emoção que é mais comum em mim é',
		'alternativas' => array(
			'Ansiedade por não ser o correto',
			'Orgulho por não ser o primeiro',
			'Medo por perder alguém',
			'Raiva'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			3, // Visionário
			0, // Orientador
		)
	),
	array(
		'pergunta' => 'Os excessos causam em mim a tendência',
		'alternativas' => array(
			'De ficar empacado e não conseguir progredir naquilo que não tive êxito',
			'De alto nível de stress, ansiedade e frustração',
			'Ter problemas nos relacionamentos',
			'Imaginar expectativas inviáveis'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			0, // Orientador
			3, // Visionário
		)
	),
	array(
		'pergunta' => 'Tenho mais facilidade nas atividades quando:',
		'alternativas' => array(
			'Estão seqüenciadas',
			'Estão classificadas',
			'Tem pessoas agregadas',
			'Não tem uma ordem, são analógicas e vêm da minha cabeça'
		),
		'correspondencia_caracteristica' => array(
			1, // Organizador
			2, // Desafiador
			3, // Visionário
			0, // Orientador
		)
	),
);
