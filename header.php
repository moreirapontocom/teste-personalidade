<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css"/>
	<link rel="stylesheet" href="style.css" type="text/css"/>

	<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<link rel="Shortcut Icon" href="img/favicon.ico" type="image/x-icon"/>

    <title>Equipe Descomplicada</title>

	<?php include('perguntas.php') ?>

</head>
<body>

	<header>
		<div class="container text-center">
			<div class="row">
				<div class="col col-xs-12 col-md-4 col-md-offset-4">
					<img src="img/logo.png" alt="Equipe Descomplicada" class="img-responsive">
				</div>
			</div>
		</div>
	</header>

	<div class="container">
