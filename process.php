<?php include('header.php'); ?>

<?php
$total_questions = count($teste);
$total_answers = count($_POST);

if ( $total_questions != $total_answers ) {

	echo '<div class="text-center">';
		echo '<strong>Você precisa responder todas as perguntas.</strong>';
		echo '<br><br>';
		echo '<a href="javascript:history.back()" class="btn btn-default btn-lg">&laquo; Voltar e continuar respondendo</a>';
	echo '</div>';

} else {

	$answers_sto = '';

	for ( $i=0;$i<$total_answers;++$i ) :

		// echo 'a pergunta '.$i.' ('.$teste[$i]['pergunta'].') está marcada na alternativa '.$_POST[$i].' = '.$caractetisticas[$_POST[$i]];
		// echo '<br>';

		$answers_sto[$_POST[$i]] = ++$answers_sto[$_POST[$i]];

	endfor;

	usort($answers_sto);

	$iam = array_shift(array_values($answers_sto)); // first array value
	$key = key($answers_sto); // First index of DESC ordened array
	?>

	<h1 class="text-center">RESULTADO</h1>

	<div class="row">
		<div class="col col-xs-12 col-md-8 col-md-offset-2 text-center margin-bottom-60">
			<img class="img-responsive margin-bottom-60" src="img/<?php echo $imagens[$key]; ?>.png" alt="<?php echo $caractetisticas[$key]; ?>">

			<div class="text-justify">
				<?php echo $descricao[$key]; ?>
			</div>
		</div>
	</div>

	<?php

}
?>

<?php include('footer.php') ?>
