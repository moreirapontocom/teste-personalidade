	</div><!-- /.container -->

	<footer>
		<div class="container text-center">
			&copy; <?php echo date('Y') ?>
		</div>
	</footer>

	<script>
		$(function() {
			$('label').click(function() {
				var _this = $(this),
					icon = $('i#icon-' + _this.attr('data-i'));

				icon.removeClass('hidden');
			});
		});
	</script>

</body>
</html>
